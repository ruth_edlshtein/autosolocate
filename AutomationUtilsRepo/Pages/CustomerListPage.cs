﻿using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Pages
{
    class CustomerListPage
    {
        //public IWebElement GetSpecificUser()
        //{

        //}
        public IWebElement GetEditlButton()
        {
            return FindElementHelper.GetElement(By.XPath("//a[@routerlink='edit']"));

        }

        public IWebElement GetValue(string value)
        {
            return FindElementHelper.GetElement(By.CssSelector($"option[ng-reflect-ng-value = '{value}']"));

        }
        public IWebElement GetCancellButton()
        {
            return FindElementHelper.GetElement(By.ClassName("btn-cancel"));

        }

        
        public IWebElement GetSaveButton()
        {
            return FindElementHelper.GetElement(By.ClassName("btn-success"));

        }

        public IWebElement GetCancelButton()
        {
            return FindElementHelper.GetElement(By.ClassName("btn-cancel"));
        }
        
        public IWebElement GetFirstname()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='first_name']"));
        }

        public IWebElement GetLastname()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='last_name']"));
        }
        public IWebElement GetEmailaddress()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@type='email']"));
        }
        public IWebElement GetPassword()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='password']"));
        }

        public IWebElement GetPhonenumber()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='phone']"));
        }

        public IWebElement GetStatus()
        {
            return FindElementHelper.GetElement(By.XPath("//select[@formcontrolname='status']"));
        }

        public IWebElement GeTFilterIcon()
        {
            return FindElementHelper.GetElement(By.XPath("//select[@title='Item Per Page']"));
        }



       
    }
}
