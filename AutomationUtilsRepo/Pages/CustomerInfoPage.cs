﻿using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Pages
{
    class CustomerInfoPage
    {

        //customer's details

        public IWebElement GetFirstname()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='first_name']"));
        }

        public IWebElement GetLastname()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='last_name']"));
        }
        public IWebElement GetEmailaddress()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@type='email']"));
        }
        public IWebElement GetPassword()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='password']"));
        }

        public IWebElement GetPhonenumber()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='phone']"));
        }

        public IWebElement GetStatus()
        {
            return FindElementHelper.GetElement(By.XPath("//select[@formcontrolname='status']"));
        }
        public IWebElement GetCD()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='cd']"));
        }

        public IWebElement GetSavebutton()


        {
            return FindElementHelper.GetElement(By.ClassName("btn-success"));
        }



        //public By GetErrorMessageOfmissingfirstname()
        //{
        //    return By.Id("error-p-name");
        //}

        public IWebElement GetCancellbutton()
        {
            return FindElementHelper.GetElement(By.ClassName("btn-cancel"));
        }

        //search

        public IWebElement GetSerachbutton()
        {
            return FindElementHelper.GetElement(By.ClassName("page-filter-icon"));

        }
        public IWebElement GetUserNameFieldSearch()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='username']"));

        }

        public IWebElement GetSearchButtonInSearch()
        {
            return FindElementHelper.GetElement(By.ClassName("btn-primary"));

        }

        //Table
        public IWebElement GetTable()
        {
            return FindElementHelper.GetElement(By.ClassName("app-table"));

        }

        public IWebElement GetHeader()
        {
            return FindElementHelper.GetElement(By.XPath("/html/body/app-root/app-feature/section/app-user-list/div/div[2]/table/thead"));

        }

        //error messages

        public By GetbyErrorlastname()
        {
            return By.XPath("//p[contains(.,' Last Name is required.')]");
        }


        public By GetbyErrorFirsttname()
        {
            return By.XPath("//p[contains(.,'First Name is required')]");
        }


        public By GetbyErrorEmailname()
        {
            return By.XPath("//p[contains(.,'Email is required.')]");
        }
        public By GetbyErrorPassword()
        {
            return By.XPath("//p[contains(.,'Password is required.')]");
        }


        public IWebElement GetBackIcon()
        {
            return FindElementHelper.GetElement(By.ClassName("page-back-icon"));
        }
    }
}
