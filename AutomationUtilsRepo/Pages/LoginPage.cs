﻿using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Pages
{
    public class LoginPage
    {
        public IWebElement GetEmailInput()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@type='email']"));
        }

        public IWebElement GetPasswordInput()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@type='password']"));
        }

        public IWebElement GetContinueButton()
        {
            return FindElementHelper.GetElement(By.XPath("//button[@type='submit']"));
        }
    }
}
