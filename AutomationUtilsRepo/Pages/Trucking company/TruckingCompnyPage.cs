﻿using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Pages.Trucking_company
{
    class TruckingCompnyPage
    {
        public IWebElement GetTruckingCompanyButton()
        {
            return FindElementHelper.GetElement(By.XPath("//a[@href='#/admin/company']"));      
        }

        //Create - edit trucking Company

        public IWebElement GetCreateTruckingCompanyButton()
        {
            return FindElementHelper.GetElement(By.XPath("//a[@href='#/admin/company/create']"));
        }

        public IWebElement GetNameField()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='name']"));
        }

        public IWebElement GetPhoneField()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='phone']"));
        }

        public IWebElement GetEmailField()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='email']"));
        }

        public IWebElement GetAddressField()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formcontrolname='address']"));
        }

        public IWebElement GetROleTamplateField()
        {
            return FindElementHelper.GetElement(By.XPath("//select[@formcontrolname='role_tmp_id']"));
        }

        public IWebElement GetROleTamplateFieldContent()
        {
            return FindElementHelper.GetElement(By.XPath("//label[@formcontrolname='role_tmp_id']"));
        }

        public IWebElement GetSaveButton()
        {
            return FindElementHelper.GetElement(By.XPath("//button[@type='submit']"));
        }

        public IWebElement GetCancelButton()
        {
            return FindElementHelper.GetElement(By.XPath("//button[@type='button']"));
        }

        //table 

        public IWebElement GetTable()
        {
            return FindElementHelper.GetElement(By.ClassName("app-table"));
        }

        public IWebElement GetHeader()
        {
            return FindElementHelper.GetElement(By.XPath("/html/body/app-root/app-feature/section/app-company-list/div/div[2]/table/thead"));
        }


        //error messages

        public By GetByEmptyNameErrorMessage()
        {
            return By.XPath("//p[contains(.,'Name is required.')]") ;
        }

        public By GetByEmptyEmailErrorMessage()
        {
            return By.XPath("//p[contains(.,'Email is required.')]");
        }

        public By GetByEmptyAddressErrorMessage()
        {
            return By.XPath("//p[contains(.,'Address is required.')]");
        }

        public By GetByEmptyRoleTamplateErrorMessage()
        {
            return By.XPath("//p[contains(.,'Role Template is required.')]");
        }




        //Search section 

        public IWebElement GetSearchButton()
        {
            return FindElementHelper.GetElement(By.ClassName("page-filter-icon"));
        }

        public IWebElement GetNmaeFieldInSearch()
        {
            return FindElementHelper.GetElement(By.XPath("//input[@formontrolname='name']"));
        }

        public IWebElement GetSearchButtonInSearch()
        {
            return FindElementHelper.GetElement(By.ClassName("btn-primary"));
        }



        






    }
}
