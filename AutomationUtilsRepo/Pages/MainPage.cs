﻿using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Pages
{
    public class MainPage
    {

        //Profile
        public IWebElement GetProfileArea()
        {
            return FindElementHelper.GetElement(By.XPath("//ul[@class='navbar-nav ml-auto profile']"));
        }

        public IWebElement GetProfileDropDown()
        {
            return FindElementHelper.GetElement(By.XPath("//*[@class='profile-avatar']/parent::a"));
        }



        //Change language
        public IWebElement GetlanguageButton()
        {
            return FindElementHelper.GetElement(By.XPath("//ul[@class='navbar-nav ml-auto']"));
        }

        public IWebElement GetEnglishHebrewlanguageDropDown()
        {
            return FindElementHelper.GetElement(By.XPath("//ul[@role='menu']"));
        }
    }
}

