﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using RelevantCodes.ExtentReports;
using System.IO;
using Seldat.AutomationTools.Infra.Const;
using AutomationUtilsRepo.Infra.Const;
using Seldat.AutomationTools.Infra.Report;
using System.Reflection;
using AutomationUtilsRepo.Configuration;
using System;
using OpenQA.Selenium.Remote;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium.Interactions;
using Seldat.AutomationTools.Infra.Helper;

namespace AutomationUtilsRepo.Tests
{
    /// <summary>
    /// This attribute that marks a class that contains tests 
    /// and is inherited by any derived classes
    /// This means we do not have to worry about this in our test classes
    /// </summary>
    [TestFixture]
    public abstract class SeleniumTestBase
    {
        internal static IWebDriver driver;
        // internal static Customer myCustomer = CustomerFactory.Get();
        private static ReportingTasks _reportingTasks;
        public static ReportingManager reportingManager;
        //Run 1 time before the running.
        [OneTimeSetUp]
        public void InitBeforeRunning()
        {
            GenericConstPath.ProjectName = ConstPath.ProjectName;
            GenericConstPath.MachineName = ConstPath.MachineName;
            if (System.IO.File.Exists(GenericConstPath.CurrentReport))
                GeneralHelper.EmptyFolder(GenericConstPath.CurrentReport);//deleting old reports. 
        }

        ///<summary>
        ///Run Before every Test and setup Tests.
        ///</summary>
        [SetUp]
        [TestInitialize]
        public void TestSetup()
        {
            if (reportingManager == null)
                reportingManager = new ReportingManager();
            string testMethodName = NUnit.Framework.TestContext.CurrentContext.Test.MethodName;
            _reportingTasks = new ReportingTasks(ReportingManager.Instance);
            //     _reportingTasks.InitializeTest();
            BeginExecution();
            DriverHelper.Driver = driver;
            StepReport.Driver = driver;
            driver.Manage().Window.Maximize();
        }

        /// <summary>
        /// Runs after every Test and Cleans up Test.
        /// </summary>
        [TearDown]
        [TestCleanup]
        public void TestCleanUp()
        {
            _reportingTasks.FinalizeTest();
            //driver.Manage().Cookies.DeleteAllCookies();
            //object testListObj = (ReportingManager.Instance.GetType().GetProperty("TestList", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)).GetValue(ReportingManager.Instance);
            //List<RelevantCodes.ExtentReports.ExtentTest> testList = (List<RelevantCodes.ExtentReports.ExtentTest>)testListObj;
            //FieldInfo singleTest = testList.Last().GetType().GetField("_testStatus", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            ExitExecution();
            //if ((LogStatus)singleTest.GetValue(testList.Last()) == LogStatus.Fail)
            //    NUnit.Framework.Assert.Fail();
        }

        /// <summary>
        /// Begin execution of tests
        /// </summary>
        /// 

        public static void BeginExecution()
        {
            ExtentReports extentReports = ReportingManager.Instance;
            extentReports.LoadConfig(Directory.GetParent(NUnit.Framework.TestContext.CurrentContext.TestDirectory).Parent.FullName + "\\extent-config.xml");
            //Note we have hardcoded the browser, we will deal with this later
            extentReports.AddSystemInfo("Browser", "Chrome");
            _reportingTasks = new ReportingTasks(extentReports);
            StepReport.ReportingTasks = _reportingTasks;
            driver = DriverHelper.ChooseBrowser(BrowserType.Chrome);
            try
            {
                if ((((RemoteWebDriver)driver).Capabilities.BrowserName) != BrowserType.Chrome.ToString())
                {
                    Actions builder = new Actions(driver);
                    builder.SendKeys(Keys.F11);
                    builder.Perform();
                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
                    DriverHelper.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            driver.Navigate().GoToUrl(ConfigReader.GetEnviroment());
            FindElementHelper.Driver = driver;
        }


        /// <summary>
        /// Finish Execution of tests
        /// </summary>
        public static void ExitExecution()
        {
            if (driver != null)
            {
                driver.Close();
                driver.Quit();
            }
        }

        //Run 1 time after the running.
        [OneTimeTearDown]
        public void CreateDashboard()
        {
            try
            {
                driver = DriverHelper.ChooseBrowser();
                DriverHelper.Driver = driver;
                StepReport.Driver = driver;
                FindElementHelper.Driver = driver;
                driver.Manage().Window.Maximize();
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
                _reportingTasks.TakeScreenshotOfReportDashboard();
            }
            catch(Exception ex)
            {
                //continue
            }
            finally
            {
                ExitExecution();
            }
        }
    }
}


