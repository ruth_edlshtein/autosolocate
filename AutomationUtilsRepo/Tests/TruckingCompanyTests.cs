﻿using AutomationUtilsRepo.BusinessProcesses;
using AutomationUtilsRepo.Infra.Components;
using AutomationUtilsRepo.Infra.Enums;
using AutomationUtilsRepo.Pages.Trucking_company;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using RelevantCodes.ExtentReports;
using Seldat.AutomationTools.Infra.Helper;
using Seldat.AutomationTools.Infra.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Tests
{
    [TestClass]
    class TruckingCompanyTests : SeleniumTestBase
    {
        Dictionary<string, bool> isConditions = new Dictionary<string, bool>();
        StepReport stepReport = new StepReport();
        ReportStatus reportStatus = new ReportStatus();
        bool isTrue;


       // [Test]
       //[Retry(5)]
       // public void CreateNewTruckingCompany()
       // {

       //     #region Initialize
       //     LoginBP loginBP = new LoginBP();
       //     SideBar sideBar = new SideBar();
       //     TruckingCompany truckingCompany = new TruckingCompany();
       //     TruckingCompanyBP truckingCompanyBP = new TruckingCompanyBP();
       //     TruckingCompnyPage truckingCompanyPage = new TruckingCompnyPage();
       //     TopMenu topMenu = new TopMenu();           
       //     #endregion
       //     try
       //     {
       //         #region Step 1 - Login as Super Admin
       //         loginBP.Login(UserType.Admin);
       //         //topMenuBP.ChanageLanguage();
       //         Thread.Sleep(5000);
       //         isTrue = loginBP.IsUserLoggedIn("Seldat Admin");
       //         stepReport.Report("Step 1- Login as admin", "login success", reportStatus.Status(isTrue));
       //         #endregion

       //         #region Step 2 - Open Trucking Company tab
       //         Thread.Sleep(2000);
       //         sideBar.ClickOnAdmin();
       //         Thread.Sleep(2000);
       //         truckingCompany.ClickOnTruckingCompanyButton();
       //         Thread.Sleep(1000);
       //         stepReport.Report("Step2- Open Trucking Company tab", "Trucking company page is displayed", LogStatus.Pass);
       //         #endregion

       //         #region Step 3 - Click on create Trucking Company and set inputs

       //         truckingCompany.ClickOnCreateTruckingCompanyButton();
       //         Thread.Sleep(5000);
       //         truckingCompanyBP.SaveNewTrucking("חברה להפצה8", "0548438364", "trucking@Company.com", "ירושלים 6 בני ברק");
       //         Thread.Sleep(500);
       //         //TruckingCompanyTable truckingCompanyTable = new TruckingCompanyTable();
       //         //isTrue = truckingCompanyTable.IsTruckingCompanyExist("חברה להפצה7");
       //         stepReport.Report("Step3- Click on create Trucking Company and set inputs", "new Trucking company was created", LogStatus.Pass);
       //         #endregion

       //         #region Step 4 - cancel Creation 
       //         Thread.Sleep(2000);
       //         truckingCompany.ClickOnCreateTruckingCompanyButton();
       //         Thread.Sleep(1000);
       //         truckingCompanyBP.CancelNewTrucking("חברה להפצה ביטול", "0548438364", "trucking@Company.com", "ירושלים 6 בני ברק");
       //         isTrue = truckingCompanyBP.CheckingUrl();               
       //         stepReport.Report("Step 4- cancel Creation", "trucking company was not created", reportStatus.Status(isTrue));
       //         #endregion

       //         #region Step 5 - Check mandatory fields
       //         truckingCompany.ClickOnCreateTruckingCompanyButton();
       //         Thread.Sleep(1000);
       //         truckingCompany.ClickOnSaveButton();
       //         Thread.Sleep(1000);
       //         isConditions.Add("Name", truckingCompany.IsThereNameErrorMessage());
       //         isConditions.Add("Email", truckingCompany.IsThereEmailErrorMessage());
       //         isConditions.Add("Address", truckingCompany.IsThereAddressErrorMessage());
       //         isConditions.Add("Phone", truckingCompany.IsThereRoleTamplateErrorMessage());
       //         stepReport.Report("Step2 - first time only active customers in table", ReportHelper.ShowResult(isConditions), reportStatus.Status(ReportHelper.IsTrueConditions(isConditions)));
       //         #endregion



       //         //#region Step  - Open Search Bar
       //         //Thread.Sleep(1000);
       //         //truckingCompany.ClickOnSearchButton();
       //         //Thread.Sleep(1000);
       //         //stepReport.Report("Step - Open Search Bar", "search bar was opened", LogStatus.Pass);
       //         //#endregion
       //     }
       //     catch (Exception e)
       //     {
       //         stepReport.Report("TestFailed", "failed on exception", e.Message, LogStatus.Fail, true);
       //     }

       // }
    }
}
