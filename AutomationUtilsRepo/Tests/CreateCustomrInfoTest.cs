﻿using AutomationUtilsRepo.BusinessProcesses;
using AutomationUtilsRepo.Infra.Components;
using AutomationUtilsRepo.Infra.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using RelevantCodes.ExtentReports;
using Seldat.AutomationTools.Infra.Helper;
using Seldat.AutomationTools.Infra.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Tests
{
    [TestClass]
    class CreateCustomrInfoTest : SeleniumTestBase

    {
        CustomerInfoBP customerinfoBP = new CustomerInfoBP();
        CustomerInfo customerinfo = new CustomerInfo();
        StepReport stepReport = new StepReport();
        ReportStatus reportStatus = new ReportStatus();
        CustomerTable customersTable = new CustomerTable();

        Dictionary<string, bool> isConditions = new Dictionary<string, bool>();


        bool isTrue;

        [Test]
        [Retry(5)]


        public void Createcustomerinfo()
        {

            try
            {
                #region LOGIN
                LoginBP loginBp = new LoginBP();
                loginBp.Login(UserType.Admin);
                Thread.Sleep(1000);
                isTrue = loginBp.IsUserLoggedIn("Seldat Admin");
                stepReport.Report("Step1- Login as admin", "login passed successfully", reportStatus.Status(isTrue));
                #endregion


                #region Step 1- Click on admin tab
                customerinfoBP.ClickOnAdmin();
                Thread.Sleep(500);
                stepReport.Report("Step2- Click on admin tab", " Admin page is opened", reportStatus.Status(isTrue));

                #endregion

                #region Step 2- Click on customerstab
                customerinfoBP.Clickoncustomers();
                Thread.Sleep(500);
                stepReport.Report("Step3- Click on customers tab", " Customer page is opened", reportStatus.Status(isTrue));

                #endregion

                #region Step 3- Check mandatory field
                //customerinfoBP.ClickonnewcustomerButton();
                //Thread.Sleep(2000);
                //customerinfo.ClearPassword();
                //customerinfo.InsertCD("34535");
                //Thread.Sleep(1000);
                //customerinfo.ClickSave();
                //Thread.Sleep(1000);
                //isConditions.Add("First Name", customerinfoBP.IsThereErrorFirstname());
                //isConditions.Add("Email.", customerinfoBP.IsThereErrorEmailname());
                //isConditions.Add("Last Name", customerinfoBP.IsThereErrorLastname());
                //isConditions.Add("Password Name", customerinfoBP.IsThereErrorPassword());
                //Thread.Sleep(1000);
                //customerinfo.ClickCancel();
                //stepReport.Report("Step4 -Check mandatory fields", "Mandatory field is reqiuered", ReportHelper.ShowResult(isConditions), reportStatus.Status(ReportHelper.IsTrueConditions(isConditions)));
                //EndStep();
                #endregion

                #region Step 3- Missing validataion 
                //customerinfoBP.ClickonnewcustomerButton();
                //Thread.Sleep(2000);
                //customerinfo.ClearPassword();
                //customerinfo.InsertCD("34535");
                //Thread.Sleep(1000);
                //customerinfo.ClickSave();
                //Thread.Sleep(1000);
                //isConditions.Add("First Name", customerinfoBP.IsThereErrorFirstname());
                //isConditions.Add("Email.", customerinfoBP.IsThereErrorEmailname());
                //isConditions.Add("Last Name", customerinfoBP.IsThereErrorLastname());
                //isConditions.Add("Password Name", customerinfoBP.IsThereErrorPassword());
                //Thread.Sleep(1000);
                //customerinfo.ClickCancel();
                //stepReport.Report("Step4 -Check mandatory fields", "Mandatory field is reqiuered", ReportHelper.ShowResult(isConditions), reportStatus.Status(ReportHelper.IsTrueConditions(isConditions)));
                //EndStep();
                #endregion

                #region Step4-   click on cancel button 
                Thread.Sleep(1000);
                customerinfoBP.ClickonnewcustomerButton();
                Thread.Sleep(1000);
                customerinfoBP.CancelNewCustomer();
                isTrue= customerinfoBP.Checkingurl("list");
                stepReport.Report("Step4- Click on cancel", "customer isn't created", reportStatus.Status(isTrue));
                #endregion

                #region Step5- Create and save customer input
                Thread.Sleep(1000);
                customerinfoBP.ClickonnewcustomerButton();
                Thread.Sleep(1000);
                string name = customerinfoBP.SaveNewCustomer();
                Thread.Sleep(5000);
                customerinfoBP.SearchCustomerByUserName(name);
                Thread.Sleep(50000);
                customersTable.SetTable();
                isTrue = customersTable.IsTableContainsData();

                //stepReport.Report("Step5- Save customer input", "customer creation passed successfully", reportStatus.Status(isTrue));


                #endregion


                #region Step 6-click on back icon 

                customerinfo.ClickonLeftarrow();
                isTrue = customerinfoBP.Checkingurl("admin");
                stepReport.Report("Step 6- Click on back icon ", "", reportStatus.Status(isTrue));
                 #endregion


                #region -------
                //customerinfoBP.ClickonnewcustomerButton();
                //Thread.Sleep(2000);
                //customerinfo.ClearPassword();
                //customerinfo.ClickSave();
                //stepReport.Report("Step4- Click on Save", "success", reportStatus.Status(isTrue));

                //isTrue = customerinfoBP.IsThereErrorFirstname();

                //stepReport.Report("Step4- first name", "success", reportStatus.Status(isTrue));
                //customerinfoBP.ClickonnewcustomerButton();
                //Thread.Sleep(2000);
                //customerinfo.ClearPassword();
                //customerinfo.ClickSave();
                //Thread.Sleep(1000);
                //isConditions.Add("First Name", customerinfoBP.IsThereErrorFirstname());
                //isConditions.Add("Email.", customerinfoBP.IsThereErrorEmailname());
                //isConditions.Add("Last Name", customerinfoBP.IsThereErrorLastname());
                //isConditions.Add("Password Name", customerinfoBP.IsThereErrorPassword());
                //stepReport.Report("Step4 - ErrorMessagesforMandatoryfields", ReportHelper.ShowResult(isConditions), reportStatus.Status(ReportHelper.IsTrueConditions(isConditions)));
                //EndStep();
                //#endregion

                //  #region Setinfodetails
                // customerinfoBP.SetCustomerinfo("sh181861@gmail.com", "Aa123456!", "shira", "epstein", "03670086", "7538");
                //#endregion
                //#region Setinfodetails
                //customerinfoBP.SetCustomerinfo("sh181861@gmail.com", "Aa123456!", "shira", "epstein", "03670086", "7538");
                //#endregion

                //#region Step4-  Cancel creation
                //customerinfoBP.ClickonnewcustomerButton();
                //customerinfoBP.CancelNewCustomer("sh1818b61@gmail.com", "Aa123456!", "liora", "epstein", "0356686", "79578");
                //stepReport.Report("Step4- Cancelinfodetails", "cancel success", reportStatus.Status(isTrue));
               #endregion
            }
            catch (Exception e)

            {
                stepReport.Report("TestFailed", "failed on exception", e.Message +e.StackTrace, LogStatus.Fail, true); }

            }

        public void EndStep()
        {
            isConditions.Clear();
        }
    }
    } 
