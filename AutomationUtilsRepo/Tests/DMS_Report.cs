﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Const;
using Seldat.AutomationTools.Infra.Helper;
using Seldat.AutomationTools.Infra.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Tests
{
    [TestClass]
    class DMS_Report:SeleniumTestBase
    {
        private ReportingTasks _reportingTasks;


        [Test]
        [Category("dms_report")]
        public void SaveReportAsJPG()
        {
            try
            {
                DriverHelper.NavigateToPage("file:///C:/Users/Administrator/IdeaProjects/dmscoreautotest-bdd/target/site/serenity/index.html");
                Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
                screenshot.SaveAsFile(@"C:\Users\Administrator\IdeaProjects\dmscoreautotest-bdd\dashboard.png");
            }
            catch(Exception ex)
            {
                throw new Exception("Error while convert the DMS report from HTML to JPG:  " + ex.StackTrace );
            }

        }
    }
}
