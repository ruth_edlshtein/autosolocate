﻿using AutomationUtilsRepo.BusinessProcesses;
using AutomationUtilsRepo.Infra.Components;
using AutomationUtilsRepo.Infra.Enums;
using NUnit.Framework;
using RelevantCodes.ExtentReports;
using Seldat.AutomationTools.Infra.Helper;
using Seldat.AutomationTools.Infra.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Tests
{
    class CustomerListTest:SeleniumTestBase
    {

        CustomerInfoBP customerinfoBP = new CustomerInfoBP();
        CustomerInfo customerinfo = new CustomerInfo();
        StepReport stepReport = new StepReport();
        ReportStatus reportStatus = new ReportStatus();
        CustomerTable customersTable = new CustomerTable();
        CustomerList customerList = new CustomerList();
        private bool isFalse;
        private bool isTrue;

        CustomerListBP customerListBP = new CustomerListBP();
        [Test]
        [Retry(5)]


        public void EditCustomer()
        {

            try
            {
                #region LOGIN
                LoginBP loginBp = new LoginBP();
                loginBp.Login(UserType.Admin);
                Thread.Sleep(1000);
                isTrue = loginBp.IsUserLoggedIn("Seldat Admin");
                stepReport.Report("Step1- Login as admin", "login passed successfully", reportStatus.Status(isTrue));
                #endregion

                #region Step 1 Click on admin tab
                customerinfoBP.ClickOnAdmin();
                Thread.Sleep(500);
                stepReport.Report("Step2- Click on admin tab", " Admin page is opened", reportStatus.Status(isTrue));
                #endregion

                #region Step 2- Click on customerstab
                customerinfoBP.Clickoncustomers();
                Thread.Sleep(1000);
                stepReport.Report("Step3- Click on customers tab", " Customer page is opened", reportStatus.Status(isTrue));
                #endregion

               
                #region  Step 4 -Click on specifing customer

                customersTable.ClickOnCustomerByName();
                Thread.Sleep(1000);
                stepReport.Report("Step 4- Clicking on specifing user", "user details windows is opened", reportStatus.Status(isTrue));

                #endregion




                #region  Step 5-Try to edit fields before clicking on edit  button
                isFalse =! customerListBP.IsFirstNameEnable();
                Thread.Sleep(1000);
                stepReport.Report("Step 5- Try to edit fields before clicking on edit  button", "There is no option to edit the field", reportStatus.Status(isFalse));
                #endregion


                #region  Step 6 -edit customer fields and cancel
                customerList.ClickOnEdit();
                Thread.Sleep(1000);
                //customerinfoBP.ClickonnewcustomerButton();
                Thread.Sleep(1000);
                string name = customerinfoBP.EditCustomer();
                Thread.Sleep(5000);
                customerinfoBP.CanceleditCustomer();
                isTrue = customerinfoBP.Checkingurl("list");
                stepReport.Report("Step 6 Edit customer fields and click on cancel", "the changes arent been saved", reportStatus.Status(isTrue));
                #endregion


                #region  Step 6 -edit customer fields and Save
                Thread.Sleep(1000);
                customersTable.ClickOnCustomerByName();
                Thread.Sleep(1000);
                customerList.ClickOnEdit();
                Thread.Sleep(1000);
                // customerinfoBP.ClickonnewcustomerButton();
                string name1 = customerinfoBP.EditCustomer();
                Thread.Sleep(5000);
                customerinfo.ClickSave();
                Thread.Sleep(5000);
                customerinfoBP.SearchCustomerByUserName(name1);
                Thread.Sleep(5000);
                customersTable.SetTable();
                isTrue = customersTable.IsTableContainsData();
                stepReport.Report("Step 6 Edit customer fields and click on save", "the changes are saved succeessfuly", reportStatus.Status(isTrue));
                #endregion


                //#region  Step 7 -edit customer fields and save
                //#endregion

                //#region  Step 3 - Filter the table by row
                //customerList.ChooseFilteByNum();
                //customersTable.SetTable();
                //isTrue = customersTable.FilterTableByRow(30);
                //stepReport.Report("Step 3- filter the table by row", "the table is filtered correctly", reportStatus.Status(isTrue));
                //#endregion
            }

            catch (Exception e)

            {
                stepReport.Report("TestFailed", "failed on exception", e.Message + e.StackTrace, LogStatus.Fail, true);
            }

        }








    }
}
