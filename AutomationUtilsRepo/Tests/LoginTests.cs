﻿using AutomationUtilsRepo.BusinessProcesses;
using AutomationUtilsRepo.Infra.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using RelevantCodes.ExtentReports;
using Seldat.AutomationTools.Infra.Helper;
using Seldat.AutomationTools.Infra.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Tests
{
    [TestClass]
    class LoginTests : SeleniumTestBase
    {
        LoginBP loginBP = new LoginBP();
        StepReport stepReport = new StepReport();
        ReportStatus reportStatus = new ReportStatus();
        bool isTrue;

        [Test]
        [Retry(5)]
        public void LoginAsAdminTest()
        {
            loginBP.Login(UserType.Admin);
            isTrue = loginBP.IsUserLoggedIn("Seldat Admin");
            stepReport.Report("Step1- Login as admin", "login success", reportStatus.Status(isTrue));
        }

    }
}
