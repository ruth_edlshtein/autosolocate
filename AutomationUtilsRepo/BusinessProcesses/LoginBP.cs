﻿using AutomationUtilsRepo.Infra.Components;
using AutomationUtilsRepo.Infra.Enums;
using AutomationUtilsRepo.Infra.Factories;
using AutomationUtilsRepo.Infra.Objects;
using AutomationUtilsRepo.Pages;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.BusinessProcesses
{
    public class LoginBP
    {
        Login login = new Login();
        MainPage main = new MainPage();
        TopMenuBP topMenuBP = new TopMenuBP();
        public void Login(string userName, string password)
        {
            login.InsertEmail(userName);
            login.InsertPassword(password);
            login.ClickLogin();
        }

        public void Login(UserType userType)
        {
            User user = UsersFactory.Get(userType);
            Login(user.email, user.password);
        }

        public bool IsUserLoggedIn(string userName)//Need to be in the main screen before calling this function
        {
            if (FindElementHelper.GetText(main.GetProfileDropDown()) == userName)
                return true;
            return false;
        }

        //login & change site language
        public void LoginAndChangeSiteLanguage(UserType userType)
        {
            User user = UsersFactory.Get(userType);
            Login(user.email, user.password);
            topMenuBP.ChanageLanguage();
        }


    }
}
