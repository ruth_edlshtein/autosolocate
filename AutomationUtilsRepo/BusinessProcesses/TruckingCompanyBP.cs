﻿using AutomationUtilsRepo.Infra.Components;
using AutomationUtilsRepo.Pages.Trucking_company;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.BusinessProcesses 
{
    class TruckingCompanyBP
    {
        TruckingCompany truckingCompany = new TruckingCompany();
        TruckingCompnyPage truckingCompanyPage = new TruckingCompnyPage();

        //create trucking company
        public void SetTruckinginfo(string name, string phone, string email, string address)
        {
            truckingCompany.InsertName(name);
            truckingCompany.InsertPhone(phone);
            truckingCompany.InsertEmail(email);
            truckingCompany.InsertAddress(address);
            truckingCompany.chooseTamplate();

        }
        public void SaveNewTrucking(string name, string phone, string email, string address)
        {
            SetTruckinginfo(name, phone, email, address);
            truckingCompany.ClickOnSaveButton();
        }

        public void CancelNewTrucking(string name, string phone, string email, string address)
        {
            SetTruckinginfo(name, phone, email, address);
            truckingCompany.ClickOnCancelButton();
        }


        public bool CheckingUrl()
        {
            string text = "list";
            if (DriverHelper.GetUrl().Contains(text))
                return true;
            return false;
        }






    }
}
