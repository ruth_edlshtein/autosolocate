﻿using AutomationUtilsRepo.Infra.Components;
using AutomationUtilsRepo.Pages;
using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.BusinessProcesses
{
    public class CustomerInfoBP
    {
        CustomerTable customerTable = new CustomerTable();
        CustomerInfo customerinfo = new CustomerInfo();
        CustomerInfoPage customerinfopage = new CustomerInfoPage();



        public string SetCustomerinfo()
        {
            string emailAddress = "test" + GeneralHelper.RandomMail();

            string firstname = (GeneralHelper.RandomStringOfLetters(5));
            string lastName = (GeneralHelper.RandomStringOfLetters(5));
            customerinfo.Insertfirstname(firstname);
            customerinfo.Insertlastname(lastName);
            customerinfo.InsertEmail(emailAddress);
            customerinfo.InsertPassword(GeneralHelper.RandomStringOfLetters(7));
            customerinfo.InsertPhonenumber(GeneralHelper.RandomStringOfNumbers(6));
            customerinfo.InsertCD(GeneralHelper.RandomStringOfNumbers(3));
            customerinfo.ChooseStatus();
            return firstname+ " " + lastName;
        }

        public string EditCustomerinfo()
        {
            string emailAddress = "test" + GeneralHelper.RandomMail();

            string firstname = (GeneralHelper.RandomStringOfLetters(5));
            string lastName = (GeneralHelper.RandomStringOfLetters(5));
            customerinfo.Insertfirstname(firstname);
            customerinfo.Insertlastname(lastName);
            customerinfo.InsertEmail(emailAddress);
          //  customerinfo.InsertPassword(GeneralHelper.RandomStringOfLetters(7));
            customerinfo.InsertPhonenumber(GeneralHelper.RandomStringOfNumbers(6));
            customerinfo.InsertCD(GeneralHelper.RandomStringOfNumbers(3));
            customerinfo.ChooseStatus();
            return firstname + " " + lastName;
        }
        public string SaveNewCustomer()
        {
            string newCustomerName = SetCustomerinfo();
            //Thread.Sleep(5000);

            customerinfo.ClickSave();
            return newCustomerName;
        }

        public string EditCustomer()
        {
            string newCustomerName = EditCustomerinfo();
            //Thread.Sleep(5000);

           
            return newCustomerName;
        }
        public void CancelNewCustomer()
        {
            SetCustomerinfo();
            Thread.Sleep(5000);
            customerinfo.ClickCancel();
        }



        public void CanceleditCustomer()
        {
            customerinfo.ClickCancel();


        }
        public bool Checkingurl(string word)
        {
            string text = word;
            if (DriverHelper.GetUrl().Contains(text))
                return true;
            return false;
        }

       
      

        public void ClickOnAdmin()
        {
            FindElementHelper.ClickOnElement(customerinfo.GetAdminButton());
        }

        public void Clickoncustomers()
        {
            FindElementHelper.ClickOnElement(customerinfo.Getcustomers());
        }

        public void ClickonnewcustomerButton()
        {
            FindElementHelper.ClickOnElement(customerinfo.GetnewcustomerButton());

        }
        //Error Messages
        public bool IsThereErrorEmailname()
        {
            return FindElementHelper.HasElement(customerinfopage.GetbyErrorEmailname());
        }

        public bool IsThereErrorFirstname()
        {
            return FindElementHelper.HasElement(customerinfopage.GetbyErrorFirsttname());
        }
        public bool IsThereErrorPassword()
        {
            return FindElementHelper.HasElement(customerinfopage.GetbyErrorPassword());
        }



        public bool IsThereErrorLastname()
        {
            return FindElementHelper.HasElement(customerinfopage.GetbyErrorlastname());
        }

        //Search

       
        public void SearchCustomerByUserName(string NAME)
        {
            customerinfo.ClickonSearch();
            customerinfo.InsertUserNameFieldSearch(NAME);
            
            customerinfo.ClickOnSearchButton();
        }



    }
}
