﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Infra.Const
{
    public class ConstPath
    {
        //fill in your project name
        public static string ProjectName = GetProjectName();
        public static string MachineName = GetMachineName();
        public static string ApiMechine
        {
            get
            {
                return "http://mds.seldatech.com/api/";
            }
        }
        public static string GetProjectName()
        {
            return System.AppContext.BaseDirectory.Remove(0, 12).Replace(@"\AutomationUtilsRepo\bin\Debug\", "");
        }

        public static string GetMachineName()
        {
            return System.Environment.MachineName;
        }


    }
}
