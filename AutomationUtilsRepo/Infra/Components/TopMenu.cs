﻿using AutomationUtilsRepo.Pages;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Infra.Components
{
    class TopMenu
    {
        MainPage mainPage = new MainPage(); 
        public void ClickOnLanguageButton()
        {
            FindElementHelper.ClickOnElement(mainPage.GetlanguageButton());
        }

        public void ClickOnLanguageDropDownResult()
        {
            FindElementHelper.ClickOnElement(mainPage.GetEnglishHebrewlanguageDropDown());
        }

    }
}
