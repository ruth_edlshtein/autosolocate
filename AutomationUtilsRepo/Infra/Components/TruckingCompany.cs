﻿using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationUtilsRepo.Pages.Trucking_company;

namespace AutomationUtilsRepo.Infra.Components
{
    class TruckingCompany
    {

        TruckingCompnyPage truckingCompanyPage = new TruckingCompnyPage();
        public void ClickOnTruckingCompanyButton()
        {
            FindElementHelper.ClickOnElement(truckingCompanyPage.GetTruckingCompanyButton());
        }

        public void ClickOnCreateTruckingCompanyButton()
        {
            FindElementHelper.ClickOnElement(truckingCompanyPage.GetCreateTruckingCompanyButton());
        }

        public void InsertName(string name)
        {
            FindElementHelper.SendKeys(truckingCompanyPage.GetNameField(), name);
        }
        public void InsertPhone(string phone)
        {
            FindElementHelper.SendKeys(truckingCompanyPage.GetPhoneField(), phone);
        }

        public void InsertEmail(string email)
        {
            FindElementHelper.SendKeys(truckingCompanyPage.GetEmailField(), email);
        }

        public void InsertAddress(string address)
        {
            FindElementHelper.SendKeys(truckingCompanyPage.GetAddressField(), address);
        }

        public void chooseTamplate()
        {
            FindElementHelper.SelectByIndex(truckingCompanyPage.GetROleTamplateField(), 0);
        }

        public void ClickOnSaveButton()
        {
            FindElementHelper.ClickOnElement(truckingCompanyPage.GetSaveButton());
        }

        public void ClickOnCancelButton()
        {
            FindElementHelper.ClickOnElement(truckingCompanyPage.GetCancelButton());
        }
        //error messages

        public bool IsThereNameErrorMessage()
        {
            return FindElementHelper.HasElement(truckingCompanyPage.GetByEmptyNameErrorMessage());
        }

        public bool IsThereEmailErrorMessage()
        {
            return FindElementHelper.HasElement(truckingCompanyPage.GetByEmptyEmailErrorMessage());
        }

        public bool IsThereAddressErrorMessage()
        {
            return FindElementHelper.HasElement(truckingCompanyPage.GetByEmptyAddressErrorMessage());
        }

        public bool IsThereRoleTamplateErrorMessage()
        {
            return FindElementHelper.HasElement(truckingCompanyPage.GetByEmptyRoleTamplateErrorMessage());
        }

        
        //Search section

        public void ClickOnSearchButton()
        {
            FindElementHelper.ClickOnElement(truckingCompanyPage.GetSearchButton());
        }

    }
}
