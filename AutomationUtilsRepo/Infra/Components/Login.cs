﻿using AutomationUtilsRepo.Pages;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Infra.Components
{
    public class Login
    {
        LoginPage loginPage = new LoginPage();

        public void InsertEmail(string email)
        {
            FindElementHelper.SendKeys(loginPage.GetEmailInput(), email);
        }

        public void InsertPassword(string password)
        {
            FindElementHelper.SendKeys(loginPage.GetPasswordInput(), password);
        }

        public void ClickLogin()
        {
            FindElementHelper.ClickOnElement(loginPage.GetContinueButton());
        }

    }
}
