﻿using AutomationUtilsRepo.Pages;
using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Components;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Infra.Components
{


    public class CustomerTable
    {
        Table table;

        CustomerInfoPage customerinfopage = new CustomerInfoPage();

  
        public void SetTable()
        {
            table = new Table(customerinfopage.GetTable());
        }

        public bool IsCustomerExist(string name)
        {
            if (GetIndexRowByName(name) > 0)
                return true;
            return false;
        }

        private int GetIndexRowByName(string name)
        {
            List<Tuple<IWebElement, ReadOnlyCollection<IWebElement>>> coulmn = table.GetCoulmn(1);
            for (int i = 0; i < coulmn.Count; i++)
            {
                if (coulmn[i].Item1.Text == name)
                    return i + 1;
            }
            return 0;

        }
        public bool IsTableContainsData()
        {
           if (table.GetRow(1)[0].Item1.Text == "No Data")
                return false;
            return true;
        }

        public bool FilterTableByRow(int num)
        {
            if (table.GetNumOfRowsInTable() == num)
                     return true;
            return false;
        }

        public void ClickOnCustomerByName()
        {
            SetTable();
            List<Tuple<IWebElement, ReadOnlyCollection<IWebElement>>> row = table.GetRow(3);
            FindElementHelper.ClickOnElement(row[1].Item2[0]);


        }

       

    }
}
