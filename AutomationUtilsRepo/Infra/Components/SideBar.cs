﻿using AutomationUtilsRepo.Pages;
using AutomationUtilsRepo.Pages.Tabs;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Infra.Components
{
    class SideBar
    {

        TabsPage tabsPage = new TabsPage();
        public void ClickOnAdmin()
        {
            FindElementHelper.ClickOnElement(tabsPage.GetAdminButton());
        }
    }
}
