﻿using AutomationUtilsRepo.Pages;
using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Infra.Components
{
    class CustomerList
    {
        CustomerListPage customerlistpage = new CustomerListPage();


        public void Insertfirstname(string firstname)
        {
            FindElementHelper.SendKeys(customerlistpage.GetFirstname(), firstname);
        }

        public void Insertlastname(string lastname)
        {
            FindElementHelper.SendKeys(customerlistpage.GetLastname(), lastname);
        }

        public void InsertEmail(string email)
        {
            FindElementHelper.SendKeys(customerlistpage.GetEmailaddress(), email);
        }

      
        public void InsertPhonenumber(string Phonenumber)
        {
            FindElementHelper.SendKeys(customerlistpage.GetPhonenumber(), Phonenumber);
        }

        public void ClickOnSave()
        {
            FindElementHelper.ClickOnElement(customerlistpage.GetSaveButton());
        }


        public void ClickOnEdit()
        {
            FindElementHelper.ClickOnElement(customerlistpage.GetEditlButton());
        }

        public void ClickOnCancel()
        {
            FindElementHelper.ClickOnElement(customerlistpage.GetCancellButton());
        }


        public void ChooseFilteByNum()
        {
            FindElementHelper.ClickOnElement(customerlistpage.GeTFilterIcon());
            FindElementHelper.ClickOnElement(customerlistpage.GetValue("30"));

        }


     

    }
}
