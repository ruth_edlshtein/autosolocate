﻿using Seldat.AutomationTools.Infra.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using AutomationUtilsRepo.Pages;
using System.Collections.ObjectModel;
using Seldat.AutomationTools.Infra.Helper;
using AutomationUtilsRepo.Pages.Trucking_company;

namespace AutomationUtilsRepo.Infra.Components
{
    class TruckingCompanyTable
    {
        Table truckingCompanyTable;
        Table headerTable;

        TruckingCompnyPage truckingCompanyPage = new TruckingCompnyPage();

        public TruckingCompanyTable()
        {
            truckingCompanyTable = new Table(truckingCompanyPage.GetTable());
            headerTable = new Table(truckingCompanyPage.GetHeader());
        }

        private void SetTable()
        {
            truckingCompanyTable = new Table(truckingCompanyPage.GetTable());
            headerTable = new Table(truckingCompanyPage.GetHeader());
        }

        //private int GetIndexCoulmnByHeaderName(string headerName)
        //{
        //    headerTable = new Table(truckingCompanyPage.GetTruckingCompanyTableHeader());
        //    List<Tuple<IWebElement, ReadOnlyCollection<IWebElement>>> rows = headerTable.GetRow(1);

        //    for (int i = 0; i < rows.Count; i++)
        //    {
        //        if (rows[i].Item1.Text == headerName)
        //            return i + 1;
        //    }
        //    return 0;
        //}

        //private int GetIndexRowByName(string truckingCompanyName)
        //{
        //    int indexCoulmn = GetIndexCoulmnByHeaderName("Name");
        //    truckingCompanyTable = new Table(truckingCompanyPage.GetTruckingCompanyTableHeader());
        //    List<Tuple<IWebElement, ReadOnlyCollection<IWebElement>>> coulmn = truckingCompanyTable.GetCoulmn(indexCoulmn);
        //    for (int i = 0; i < coulmn.Count; i++)
        //    {
        //        if (coulmn[i].Item1.Text == truckingCompanyName)
        //            return i + 1;
        //    }
        //    return 0;

        //}

        //public List<Tuple<IWebElement, ReadOnlyCollection<IWebElement>>> GetRow(string truckingCompanyName)
        //{
        //    int indexRow = GetIndexRowByName(truckingCompanyName);
        //    return truckingCompanyTable.GetRow(indexRow);
        //}

        //public string GetNameByMail(string truckingCompanyName)
        //{
        //    List<Tuple<IWebElement, ReadOnlyCollection<IWebElement>>> row = GetRow(truckingCompanyName);
        //    return row[0].Item2[0].Text;
        //}

        //public bool IsTruckingCompanyExist(string truckingCompanyName)
        //{
        //    if (GetIndexRowByName(truckingCompanyName) > 1)
        //        return true;
        //    return false;
        //}

    }
}
