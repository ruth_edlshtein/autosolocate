﻿using AutomationUtilsRepo.Pages;
using OpenQA.Selenium;
using Seldat.AutomationTools.Infra.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Infra.Components
{
    class CustomerInfo
    {
        CustomerInfoPage customerinfopage = new CustomerInfoPage();

        public void Insertfirstname(string firstname)
        {
            FindElementHelper.SendKeys(customerinfopage.GetFirstname(), firstname);
        }

        public void Insertlastname(string lastname)
        {
            FindElementHelper.SendKeys(customerinfopage.GetLastname(), lastname);
        }

        public void InsertEmail(string email)
        {
            FindElementHelper.SendKeys(customerinfopage.GetEmailaddress(), email);
        }

        public void InsertPassword(string Password)
        {
            FindElementHelper.SendKeys(customerinfopage.GetPassword(), Password);
        }

        public void ClearPassword()
        {
            FindElementHelper.ClearByControlAAndBackspace(customerinfopage.GetPassword());
            FindElementHelper.Clear((customerinfopage.GetPassword()));

        }

        public void InsertPhonenumber(string Phonenumber)
        {
            FindElementHelper.SendKeys(customerinfopage.GetPhonenumber(), Phonenumber);
        }

        public void InsertCD(string CD)
        {
            FindElementHelper.SendKeys(customerinfopage.GetCD(), CD);
        }

        //search
        public void ClickonSearch()
        {
            FindElementHelper.ClickOnElement(customerinfopage.GetSerachbutton());


        }
        public void ClickonLeftarrow()
        {
            FindElementHelper.ClickOnElement(customerinfopage.GetBackIcon());


        }


        public void InsertUserNameFieldSearch(string NAME)
        {
            FindElementHelper.SendKeys(customerinfopage.GetUserNameFieldSearch(), NAME);


        }
        public void ClickOnSearchButton()
        {
            FindElementHelper.ClickOnElement(customerinfopage.GetSearchButtonInSearch());


        }

        //  public void InsertPhonenumber(string Password)
        //{
        //    FindElementHelper.SendKeys(customerinfopage.GetPassword(), Password);
        // }

        public void ChooseStatus()
        {
            FindElementHelper.SelectByIndex(customerinfopage.GetStatus(),1);
        }
        public void ClickSave()
       {
        FindElementHelper.ClickOnElement(customerinfopage.GetSavebutton());
       }

        public void ClickCancel()
        {
            FindElementHelper.ClickOnElement(customerinfopage.GetCancellbutton());
        }
        public IWebElement GetAdminButton()
        {
            return FindElementHelper.GetElement(By.XPath("//a[@href='#/admin']"));
        }

        public IWebElement Getcustomers()
        {
            return FindElementHelper.GetElement(By.XPath("//a[@href='#/admin/customer']"));
        }
        public IWebElement GetnewcustomerButton()
        {
            return FindElementHelper.GetElement(By.XPath("//a[@href='#/admin/customer/create']"));

        }


    }
}
