﻿using AutomationUtilsRepo.Infra.Enums;
using AutomationUtilsRepo.Infra.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Infra.Factories
{
    class UsersFactory
    {
        public static User Get(UserType _userType = UserType.Default)
        {
            switch (_userType)
            {
                case UserType.Default:
                case UserType.Admin:
                    return GetAdmin();
            }
            return GetAdmin();
        }

        private static User GetAdmin()
        {
            return new User("super.admin@seldatinc.com", "Development.123");
        }
    }
}
