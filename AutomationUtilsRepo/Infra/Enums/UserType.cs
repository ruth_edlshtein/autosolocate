﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationUtilsRepo.Infra.Enums
{
    public enum UserType
    {
        Admin, 
        Driver,
        Default
    }
}
